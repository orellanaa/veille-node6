const express = require('express');
const bodyParser= require('body-parser')
const MongoClient = require('mongodb').MongoClient // le require pour MongoDB
const ObjectID = require('mongodb').ObjectID;
const fs = require("fs");
var app = express();
var util = require("util");

app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static('public'));



/* on associe le moteur de vue au module «ejs» */

app.set('view engine', 'ejs'); // générateur de template



/* ROUTE ACCUEIL */

app.get('/accueil', (req, res) => {
 console.log('accueil') 
  
  res.render('accueil.ejs')

})


/* ROUTE ADRESSE */

app.get('/adresses', (req, res) => {
 console.log('adresses') 
 var cursor = db.collection('adresse')
                .find().toArray(function(err, resultat){
 if (err) return console.log(err)
 // transfert du contenu vers la vue index.ejs (renders)
console.log('util = ' + util.inspect(resultat));

 // affiche le contenu de la BD
 res.render('adresses.ejs', {adresses: resultat})
 }) 

})


/* DÉTRUIRE */

app.get('/detruire/:id', (req, res) => {
 var id = req.params.id
 console.log(id)
 db.collection('adresse')
 .findOneAndDelete({"_id": ObjectID(req.params.id)}, (err, resultat) => {

if (err) return console.log(err)
 res.redirect('/adresses')  // redirige vers la route qui affiche la collection
 })
})


/* AJOUTER  */

app.post('/ajouter', (req, res) => {
	db.collection('adresse').insert(req.body, (err, result) => {
	if (err) return console.log(err)
		console.log('sauvegarder dans la BD')
		res.redirect('/adresses')
	})
})

/* MODIFIER */

app.post('/modifier', (req, res) => {
	console.log("modifier");
	req.body._id = ObjectID(req.body._id)
	db.collection('adresse').save(req.body, (err, result) => {
		if (err) return console.log(err)
		console.log('sauvegarder dans la BD')
		res.redirect('/adresses')
	})
})


/* TRIER */

app.get('/trier/:cle/:ordre', (req, res) => {
let cle = req.params.cle;

console.log(cle);

 let ordre = (req.params.ordre == 'asc' ? 1 : -1)
 

 console.log('cle=' + cle)
 console.log('ordre=' + ordre)
 
 
 let cursor = db.collection('adresse').find().sort(cle,ordre).toArray(function(err, resultat){

 	
ordre = (req.params.ordre == 'asc' ? 'desc' : 'asc')

console.log(ordre);

 res.render('adresses.ejs', {adresses: resultat,cle, ordre})

})

})

/* ROUTE ACCUEIL */

app.get('/', (req, res) => {
 console.log('accueil') 
 var cursor = db.collection('adresse')
                .find().toArray(function(err, resultat){
 if (err) return console.log(err)
 // transfert du contenu vers la vue index.ejs (renders)
console.log('util = ' + util.inspect(resultat));

 // affiche le contenu de la BD
 res.render('gabarit.ejs', {adresses: resultat})
 }) 

})

/* SERVEUR MONGODB */

let db // variable qui contiendra le lien sur la BD

MongoClient.connect('mongodb://127.0.0.1:27017', (err, database) => {
 if (err) return console.log(err)
 db = database.db('carnet_adresse')
// lancement du serveur Express sur le port 8081
 app.listen(8081, () => {
 console.log('connexion à la BD et on écoute sur le port 8081')
 })
})




